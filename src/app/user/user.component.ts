import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from '../interface/users';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() selectedUser: User;
  @Output() selectUserToApp = new EventEmitter();

  public users: Array<User>;
  public formData: any;
  public name: string;
  public document: string;
  public email: string;
  protected baseApiUrl = 'http://0.0.0.0:3000';

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit() {
    this.getUsers();
    this.formData = new FormGroup({
      name: new FormControl(''),
      document: new FormControl(''),
      email: new FormControl('')
   });
  }

  saveUser(data) {
    this.httpClient.post(`${this.baseApiUrl}/user`, data).subscribe((res: any) => {
      this.getUsers();
    })
  }

  getUsers() {
    this.httpClient.get(`${this.baseApiUrl}/user`).subscribe((res: any[]) => {
      this.users = res;
    })
  }

  selectUser(user: User) {
    this.selectedUser = user;
    this.selectUserToApp.emit(user);
  }
}
