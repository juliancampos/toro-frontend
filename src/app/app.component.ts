import { Component } from '@angular/core';
import { UserComponent } from './user/user.component';
import { AccountComponent } from './account/account.component';
import { User } from './interface/users';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserComponent, AccountComponent]
})
export class AppComponent {
  title = 'PageInvestimentos';
  user: User = undefined;

  receiveUser(data) {
    this.user = data;
  }
}
