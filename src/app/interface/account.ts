export interface Account {
  id: number;
  userId: number;
  type: string;
  balance: number;
  description: string;
}

