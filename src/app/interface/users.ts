import { Account } from './account';

export interface User {
  id: number;
  name: string;
  document: string;
  email: string;
  accounts: Array<Account>;
}