import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from '../interface/users';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  @Input() selectedUser;

  protected users: Array<User>;
  public formData: any;
  public name: string;
  public document: string;
  public email: string;
  public action: string = '';
  public aportValue: number = 0;
  public withdrawValue: number = 0;
  public quotesIndex: any = [];
  public quotesData: any = {};
  public userQuotes: any = [];
  protected baseApiUrl = 'http://0.0.0.0:3000';

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit() {
    this.getUsers();
    this.formData = new FormGroup({
      name: new FormControl(''),
      document: new FormControl(''),
      email: new FormControl('')
    });
    this.getQuotes();
  }

  getQuotes() {
    let websocket = new WebSocket('ws:0.0.0.0:8080/quotes');
    websocket.onmessage = ((e) => {
      let data = JSON.parse(e.data);
      data.timestamp = new Date(data.timestamp);
      let newKey = Object.keys(data)[0];
      if (this.quotesIndex.indexOf(newKey) === -1) {
        this.quotesIndex.push(newKey);
        this.quotesData[newKey] = data;
      } else {
        this.quotesData[newKey] = data;
      }
    })
  }

  buy(itemName, data) {
    let market = {
      buyValue: data[itemName],
      quantity: 1,
      name: itemName,
      userId: this.selectedUser.id
    }
    this.httpClient.post(`${this.baseApiUrl}/market`, market).subscribe((res: any) => {
      alert('Compra de ação realizada com sucesso!');
      this.selectedUser.accounts[this.selectedUser.accounts.length-1].balance -= Number.parseFloat(market.buyValue);
      this.getUsers();
    })
  }

  sell(item) {
    let market = {
      sellValue: this.quotesData[item.name][item.name],
      id: item.id,
      quantity: 1,
      name: item.name,
      userId: this.selectedUser.id
    }
    this.httpClient.post(`${this.baseApiUrl}/market/sell`, market).subscribe((res: any) => {
      this.selectedUser.accounts[this.selectedUser.accounts.length-1].balance += market.sellValue;
      this.getUsers();
    })
  }

  saveUser(data) {
    this.httpClient.post(`${this.baseApiUrl}/user`, data).subscribe((res: any) => {
      this.getUsers();
    })
  }

  getUsers() {
    this.httpClient.get(`${this.baseApiUrl}/user`).subscribe((res: any[]) => {
      this.users = res;
    })
  }

  getUserQuotes() {
    this.httpClient.get(`${this.baseApiUrl}/market/user/${this.selectedUser.id}`).subscribe((res: any[]) => {
      this.userQuotes = res;
    })
  }

  setAction(newAction: string) {
    this.action = newAction;
    if (this.action === 'vender') {
      this.getUserQuotes();
    }
  }

  addAporte() {
    const data = {
      value: this.aportValue,
      type: "add",
      description: "inserir",
      userId: this.selectedUser.id
    }
    this.httpClient.post(`${this.baseApiUrl}/account`, data).subscribe((res: any) => {
      alert('Aporte realizado com sucesso!');
      this.selectedUser.accounts[this.selectedUser.accounts.length-1].balance += data.value;
      this.aportValue = 0;
    })
  }

  withdraw() {
    const data = {
      value: this.withdrawValue,
      type: "out",
      description: "saque",
      userId: this.selectedUser.id
    }
    this.httpClient.post(`${this.baseApiUrl}/account`, data).subscribe((res: any) => {
      alert('Saque realizado com sucesso!');
      this.selectedUser.accounts[this.selectedUser.accounts.length-1].balance -= data.value;
      this.withdrawValue = 0;
    })
  }
}
